# CDG-UD GitLab Project

## Objectives

This project aims at providing tools for handling universal dependencies (UD) with Categorial Dependency Grammars (CDG).

## Content

1.  **CDG-pattern** contains patterns related to CDG  enabling corpus exploration with grew.

For example, http://universal.grew.fr/?custom=6282832b79f59 shows the use of CDG-2consecRight.pat content as pattern on a  french corpus.
[more explanations to come.]

2.  **CDG-transform** contains grs (graph rewriting systems) for transforming a corpus, adding CDG constructs to it.
[more files and explanations to come.]

3.  **CDG-examples** contains analysis examples:

- [formal] a formal example (from a cdg that generates the set of *a^n b^n c^n*) in the UD format
   the .conllu file can be loaded in transform.grew.fr (see Capture image)

- [more than] a sentence illustrating the CDG analysis of *more ... than* comparatives.

- [John-ran ...] a sentence with consecutive adverbs, illustrating the CDG handling of repeatable dependencies.

## References 

This  work was partly described at CLTW-LREC 2022: http://lrec-conf.org/proceedings/lrec2022/workshops/CLTW4/index.html

- [article] version (pdf): http://lrec-conf.org/proceedings/lrec2022/workshops/CLTW4/pdf/2022.cltw4-1.6.pdf

- [poster] version: https://drive.google.com/file/d/1Z4TO_FolDXVZ1e6D6t4eqxIilsTLc5L4/view"

- [.bib] (http://lrec-conf.org/proceedings/lrec2022/workshops/CLTW4/bib/2022.cltw4-1.6.bib): 
    @InProceedings{foret-bchet-bellynck:2022:CLTW4,
    author    = {Foret, Annie  and  Béchet, Denis  and  Bellynck, Valérie},
    title     = {Iterated Dependencies in a Breton treebank and implications for a Categorial Dependency Grammar},
    booktitle      = {Proceedings of the 4th Celtic Language Technology Workshop within LREC2022},
    month          = {June},
    year           = {2022},
    address        = {Marseille, France},
    publisher      = {European Language Resources Association},
    pages     = {40--46},
    abstract  = {Categorial Dependency Grammars (CDG) are computational grammars for natural language processing, defining dependency structures. They can be viewed as a formal system, where types are attached to words, combining the classical categorial grammarsâ€™ elimination rules with valency pairing rules able to define discontinuous (non-projective) dependencies. Algorithms have been proposed to infer grammars in this class from treebanks, with respect to Mel'Äuk principles. We consider this approach with experiments on Breton. We focus in particular on ''repeatable dependencies" (iterated) and their patterns. A dependency $d$ is iterated in a dependency structure if some word in this structure governs several other words through dependency d. We illustrate this approach with data in the universal dependencies format and dependency patterns written in Grew (a graph rewriting tool dedicated to applications in natural Language Processing).},
    url       = {https://aclanthology.org/2022.cltw4-1.6}
    }