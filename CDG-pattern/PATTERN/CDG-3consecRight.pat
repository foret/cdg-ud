% 3 repetitions consecutive and right of the governor
pattern { e: GOV -> DEP1;  f: GOV -> DEP2; g : GOV -> DEP3 ;
	e.label = f.label ; e.label=g.label ; 
	 GOV << DEP1 ; DEP1 << DEP2 ; DEP2 << DEP3 ;  
    }
without { g2: GOV -> DEP12 ; DEP1 << DEP12 ; DEP12 << DEP2 }
% without intermediate edge 
without { h2: GOV -> DEP23 ; DEP2 << DEP23 ; DEP23 << DEP3 }
% without intermediate edge 


