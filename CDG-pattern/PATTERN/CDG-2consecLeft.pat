% 2 repetitions consecutive and left of the governor
pattern { e: GOV -> DEP1;  f: GOV -> DEP2;  
	e.label = f.label ; 
	DEP2 << DEP1 ;  DEP1 << GOV }
without { g: GOV -> DEP12 ; DEP2 << DEP12 ; DEP12 << DEP1 }
% without intermediate edge 

