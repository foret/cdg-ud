% 3 repetitions consecutive and left of the governor
pattern { e: GOV -> DEP1;  f: GOV -> DEP2; g : GOV -> DEP3 ;
	e.label = f.label ; e.label=g.label ; 
	DEP3 << DEP2 ; DEP2 << DEP1 ;  DEP1 << GOV }
without { g2: GOV -> DEP12 ; DEP2 << DEP12 ; DEP12 << DEP1 }
% without intermediate edge 
without { h2: GOV -> DEP23 ; DEP3 << DEP23 ; DEP23 << DEP2 }
% without intermediate edge 


% Example UD_Breton : 
% global  {sent_id="wikipedia.grammar.vislvislg cg.txt:103"}
