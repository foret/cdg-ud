% marking repetitions consecutive and right of the governor : package star-mark-right
% marking repetitions consecutive and left of the governor : package star-mark-left
%    the number of stars indicates the number of consecutive repetitions up to the node 
%       (rl mode : right to left on the right, and left to right on the left))
%      (consecutive repetitions means same edge labels for the successive edges of a given governor) 
%    we add a direction indicator on the node with repetition : sdir="L|R" (for browsing)

package star-mark-right {

rule smark-init-right {
 pattern { e: GOV -> DEP1;  f: GOV -> DEP2; DEP1[!smark] ;  DEP2[!smark] ; 
	e.label = f.label ; 
	GOV << DEP2 ; DEP2 << DEP1 ; }
 without { g: GOV -> DEP12 ; DEP2 << DEP12 ; DEP12 << DEP1 }
% without intermediate edge 
 without { h: GOV -> DEP0 ;  DEP1 << DEP0 ; e.label = h.label }
% without first similar edge (on the right of GOV, from right to left, with same label)
 commands {DEP1.smark="*" ; DEP1.sdir="R";} 
 }
 
rule smark-right {
 pattern { e: GOV -> DEP1;  f: GOV -> DEP2; DEP1[smark] ; DEP2[!smark] ;  
	e.label = f.label ; 
	GOV << DEP2 ; DEP2 << DEP1 ;  }
 without { g: GOV -> DEP12 ; DEP2 << DEP12 ; DEP12 << DEP1 }
% without intermediate edge 
 commands {DEP2.smark=DEP1.smark+"*" ; DEP2.sdir="R";} 
 }

}

package star-mark-left {

rule smark-init-left {
 % 2 repetitions consecutive and left of the governor
pattern { e: GOV -> DEP1;  f: GOV -> DEP2;  DEP1[!smark] ;  DEP2[!smark] ; 
	e.label = f.label ; 
	DEP1 << DEP2 ;  DEP2 << GOV }
without { g: GOV -> DEP12 ; DEP1 << DEP12 ; DEP12 << DEP2 }
% without intermediate edge 
 without { h: GOV -> DEP0 ; DEP0 << DEP1 ; e.label = h.label }
% without first similar edge (on the left of GOV, from left to right, with same label)
 commands {DEP1.smark="*" ; DEP1.sdir="L";} 
 }
 
rule smark-left {
pattern { e: GOV -> DEP1;  f: GOV -> DEP2;  DEP1[smark] ; DEP2[!smark] ;  
	e.label = f.label ; 
	DEP1 << DEP2 ;  DEP2 << GOV }
without { g: GOV -> DEP12 ; DEP1 << DEP12 ; DEP12 << DEP2 }
% without intermediate edge 
 commands {DEP2.smark=DEP1.smark+"*" ; DEP2.sdir="L";} 
 }

}

strat main-smark-right {  Onf(star-mark-right)  }

strat main-smark-left {  Onf(star-mark-left)  }

strat main-smark { Seq( Onf(star-mark-right) , Onf(star-mark-left) ) }
