% detailing: global { is_not_projective } and filtering 

%   for the union of these 4 configurations : 
%   {  A -> C ; B -> D ; A << B ; B << C ; C << D  }
%   {  A -> C ; D -> B ; A << B ; B << C ; C << D  }
%   {  C -> A ; B -> D ; A << B ; B << C ; C << D  }
%   {  C -> A ; D -> B ; A << B ; B << C ; C << D  }

%   with a  focus on Negative Polarity as dependant 
%   but without involving the root edge 

pattern { e1: A -> C ; e2: B -> D ; e1 >< e2 ; e1.label <> "root" ; e2.label <> "root"  ; C.Polarity="Neg" }

% possible Clustering with Keys : e1.label then w2.label 

% change pattern to test also Negative Polarity as governor: 
% pattern { e1: A -> C ; e2: B -> D ; e1 >< e2 ; e1.label <> "root" ; e2.label <> "root"  ; A.Polarity="Neg" }

