% detailing: global { is_not_projective } and filtering 

%   for the union of these 4 configurations : 
%   {  A -> C ; B -> D ; A << B ; B << C ; C << D  }
%   {  A -> C ; D -> B ; A << B ; B << C ; C << D  }
%   {  C -> A ; B -> D ; A << B ; B << C ; C << D  }
%   {  C -> A ; D -> B ; A << B ; B << C ; C << D  }

%   but without involving the root edge (and avoiding permutations)  : 

pattern { e1: A -> C ; e2: B -> D ; e1 >< e2 ; e1.label <> "root" ; e2.label <> "root"  ; A << B }

% possible Clustering with Keys : e1.label then e2.label 
